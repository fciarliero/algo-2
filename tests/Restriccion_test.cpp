#include "gtest/gtest.h"

#include "../src/Restriccion.h"

namespace {
    Dato d1(5, "", true);
    Dato d2(0, "hola", false);
    string String = "string ";
    string Nat = "nat";

    TEST(restriccion_test, generadores) {
        Restriccion r1(String, d2, false);
        Restriccion r2(Nat, d1, false);
    }

    Restriccion r1(String, d2, false);
    Restriccion r2(Nat, d1, false);
    Restriccion r3(String, d2, true);
    Restriccion r4(Nat, d1, true);
    Restriccion r5(String, d2, false);

    TEST(restriccion_test, esInclusion) {
        EXPECT_EQ(r1.opcionInclusion(), false);
        EXPECT_EQ(r2.opcionInclusion(), false);
        EXPECT_EQ(r3.opcionInclusion(), true);
        EXPECT_EQ(r4.opcionInclusion(), true);
    }

    TEST(restriccion_test, valor) {
        EXPECT_EQ(r1.valor(), d2);
        EXPECT_EQ(r2.valor(), d1);
        EXPECT_NE(r2.valor(), d2);
    }

    TEST(restriccion_test, campo) {
        EXPECT_EQ(r1.campo(), String);
        EXPECT_EQ(r2.campo(), Nat);
    }

    TEST(restriccion_test, operadores) {

        ASSERT_TRUE(r1 == r5);
        ASSERT_FALSE(r1 == r2);
        ASSERT_TRUE(r2 == Restriccion(Nat, d1, false));

        ASSERT_FALSE(r1 != r5);
        ASSERT_TRUE(r1 != r2);
        ASSERT_FALSE(r2 != Restriccion(Nat, d1, false));
    }

}
//
// Created by fran on 06/09/17.
//

