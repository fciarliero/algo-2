#include "gtest/gtest.h"
#include "../src/BaD.h"
namespace {
    Registro R1({"String1", "String2", "Nat1"},
                    {datoStr("dato1"), datoStr("dato2"), datoNat(182)});
    Registro R2({"String1", "String2", "Nat1"},
                    {datoStr("dato3"), datoStr("dato4"), datoNat(742)});
    Registro R3({"String1", "String2", "Nat1"},
                    {datoStr("dato5"), datoStr("dato6"), datoNat(420)});

    Registro R4({"String1", "Nat1"},
                    {datoStr("dato6"), datoNat(2501)});
    Registro R5({"String1", "Nat1"},
                    {datoStr("dato7"), datoNat(1618)});
    Registro R6({"String1", "Nat1"},
                    {datoStr("dato8"), datoNat(1337)});

    Tabla A({"String1", "String2", "Nat1"},
               {"String1"},
               {datoStr(""), datoStr(""), datoNat(0)});

    Tabla B({"String1", "Nat1"},
                {"Nat1"},
                {datoStr(""), datoNat(0)});

    TEST(BaD_test, agregarRegistroATabla) {

   BaD base1;
         string tablaA="tablaA";
         string tablaB="tablaB";

         base1.agregarTabla(A,tablaA);
         base1.agregarTabla(B,tablaB);

         base1.agregaRegistroATabla(tablaA,R1);
         base1.agregaRegistroATabla(tablaA,R2);


        EXPECT_FALSE(base1.tablaPertenece(tablaA,B));
        EXPECT_TRUE(base1.tablaPertenece(tablaA,A));
    }
    TEST(BaD_test,indiceTabla){
        BaD base1;
            string tablaA="tablaA";
            string tablaB="tablaB";
            string tablaC="estatablanoexiste";

            base1.agregarTabla(A,tablaA);
            base1.agregarTabla(B,tablaB);

        EXPECT_EQ(base1.indiceTabla(tablaA),1);
        EXPECT_EQ(base1.indiceTabla(tablaB),2);
        EXPECT_EQ(base1.indiceTabla(tablaC),0);

    }
    TEST(BaD_test, chequearCampos){
            Registro RP1({"String1", "String2", "Nat1"},
                    {datoStr("dato1"), datoNat(69), datoNat(182)});
        BaD base1;
            string tablaA="tablaA";
            string tablaB="tablaB";
            string tablaC="estatablanoexiste";

            base1.agregarTabla(A,tablaA);
            base1.agregarTabla(B,tablaB);
        EXPECT_TRUE(base1.chequearNombreCampos(tablaA,R1));

        EXPECT_TRUE(base1.chequearCampos(tablaA, R1));
        EXPECT_FALSE(base1.chequearCampos(tablaA, RP1));
        EXPECT_FALSE(base1.chequearCampos(tablaB,R1));
        EXPECT_TRUE(base1.chequearCampos(tablaB, R4));
        EXPECT_FALSE(base1.chequearCampos(tablaB,RP1));
    }
}
//
// Created by fran on 08/09/17.
//

