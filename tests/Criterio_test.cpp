#include "gtest/gtest.h"

#include "../src/Criterio.h"
namespace {
    Dato d1(5, "", true);
    Dato d2(0, "hola", false);
    string String1 = "string1";
    string String2 = "string2";
    string String3 = "string3";
    string Nat1 = "nat1";
    string Nat2 = "nat2";

    Restriccion r1(String1, d2, false);
    Restriccion r2(Nat1, d1, false);
    Restriccion r3(String2, d2, true);
    Restriccion r4(Nat2, d1, true);


    TEST(criterio_test, generadores) {
        Criterio c1;
        c1.agregarRestriccion(r1);
        c1.agregarRestriccion(r2);

    }

    TEST(criterio_test, listaRestricciones){
        vector<Restriccion> restricciones{r1,r2,r3,r4};
        Criterio c1;
        c1.agregarRestriccion(r1);
        c1.agregarRestriccion(r2);
        c1.agregarRestriccion(r3);
        c1.agregarRestriccion(r4);
        //testea que no se agregue elemento que ya existe
        c1.agregarRestriccion(r4);

        EXPECT_EQ(c1.listaRestricciones(),restricciones);
    }

     TEST(criterio_test, cantidadRegistros) {
        Criterio c1;
        c1.agregarRestriccion(r1);
        c1.agregarRestriccion(r2);

        EXPECT_EQ(c1.cantidadDeRestricciones(),2);

        c1.agregarRestriccion(r2);
        c1.agregarRestriccion(r3);
        EXPECT_EQ(c1.cantidadDeRestricciones(),3);

    }
    TEST(criterio_test, operadores) {
        Criterio c1,c2;

        EXPECT_TRUE(c1==c2);

        c1.agregarRestriccion(r1);
        c1.agregarRestriccion(r2);
        c2.agregarRestriccion(r1);
        c2.agregarRestriccion(r2);

        EXPECT_TRUE(c1==c2);

        c1.agregarRestriccion(r3);

        EXPECT_FALSE(c1==c2);



    }


}
//
// Created by fran on 06/09/17.
//