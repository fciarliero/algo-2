//
// Created by fran on 05/09/17.
//

#include "Busqueda.h"

Busqueda::Busqueda(Criterio &c, int usos) :
            _criterio(c), _usos(usos) {}

int Busqueda::usos() const{
    return _usos;
}

Criterio Busqueda::criterio() const{
    return _criterio;
}

void Busqueda::usarBusqueda() {
    _usos++;
}

bool operator==(const Busqueda & b1, const Busqueda &b2) {
    return b1.criterio() == b2.criterio() and b1.usos() == b2.usos();
}
bool operator>(const Busqueda & b1, const Busqueda &b2) {
    return b1.usos() >= b2.usos();
}