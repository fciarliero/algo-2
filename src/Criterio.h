//
// Created by fran on 01/09/17.
//

#ifndef TP1_CRITERIO_H
#define TP1_CRITERIO_H

#include "Dato.h"
#include "Restriccion.h"
#include <vector>
#include "utils.h"

class Criterio {
public:
//generador
    void agregarRestriccion(Restriccion &r);
//observador
    vector<Restriccion> listaRestricciones()const;

    unsigned long int cantidadDeRestricciones()const;



private:
   vector<Restriccion> _restricciones;

};
bool operator==(const Criterio&,const Criterio&);
bool operator!=(const Criterio&,const Criterio&);

#endif //TP1_CRITERIO_H
