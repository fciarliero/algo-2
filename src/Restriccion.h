#ifndef TP1_RESTRICCION_H
#define TP1_RESTRICCION_H

#include <string>
#include "Dato.h"


class Restriccion {
public:
    //generador
    Restriccion(string &campo, Dato &valor, bool opcion);
    //obs
    bool opcionInclusion()const;
    //obs
    string campo()const;
    //obs
    Dato valor()const;





private:
    string _campo;
    Dato _valor;
    bool _opcion;
};
bool operator==(const Restriccion&,const Restriccion&);
bool operator!=(const Restriccion&,const Restriccion&);
#endif //TP1_RESTRICCION_H
