//
// Created by fran on 25/08/17.
//

#ifndef TP1_BAD_H
#define TP1_BAD_H

#include "RankBusqueda.h"
#include "Criterio.h"
#include "Tabla.h"
#include <string>
#include "Dato.h"
#include "Registro.h"
#include <vector>

class BaD {
public:
// generador
//agrega la tabla si esta no esta en la base de datos.
    void agregarTabla ( Tabla &t, string &nombre);
//generador
//busca en la tabla nombreTabla con el criterio
    Tabla busqueda(string &nombreTabla, const Criterio &criterio);

//con el nombre de una tabla devuelve el indice (indice + 1!!!!)
// en la que esta aparece en _tablas
    int indiceTabla(string &nombre);

// observador
// la tabla t pertenece si existe y tiene el mismo nombre.
// si la tabla existe pero el nombre es distinto se considera que no esta en la base de datos.
    bool tablaPertenece(string &nombre,Tabla &r);

//operacion
//devuelve true si los campos de la tabla son compatibles a los campos del registro.


    bool chequearCampos(string &tnombre, const Registro &r);

//operacion
//devuelve true si el nombre del campo es usado en la tabla
//se usa en chequearCampos

    bool chequearNombreCampos(string &tnombre, const Registro &r);
//operacion
//devuelve true si los tipos de datos de los campos del registro
//son los mismos que los de la tabla de nombre "tnombre"
//se usa en chequearCampos
    bool chequearTipoDatoCampos(string &tnombre, const Registro &r);

    bool registroPertenece(string &tnombre, const Registro &r);

    void agregaRegistroATabla(string &tnombre, Registro &r);


    //Devuelve true si ninguno de los registros de la tabla tiene los mismos
    // datos en las claves de la tabla que el registro a agregar.
    bool compararClaves(string &tnombre, const Registro &r);
    //Devuelve true si el registro se puede agregar a la tabla (es decir si tiene los mismos campos y no existe ningún registro con los mismos
    //datos en las claves.
    bool registroValido(string &tnombre, const Registro &r);

    Tabla busquedaEnTablaConCriterio(string nombreTabla, const Criterio &criterio);


    bool criterioValido(Criterio const &criterio, string tabla);

    vector<Criterio> criterioMasBuscado();

private:
    vector<Tabla*> _tablas;
    vector<string> _tnombres;

    //un criterio puede ser valido para multiples tablas
        //el enunciado no pide el criterio mas utilizado de una tabla, sino el de la base de datos.
    RankBusqueda _rankBusqueda;



};


#endif //TP1_BAD_H
