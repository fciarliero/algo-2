//
// Created by fran on 25/08/17.
//

#include "BaD.h"
#include "utils.h"


void BaD::agregarTabla(Tabla &r, string &nombre) {

    if (!tablaPertenece(nombre,r)) {
        _tnombres.push_back(nombre);
        _tablas.push_back(&r);
    }
}


bool BaD::tablaPertenece(string &nombre, Tabla &r) {
    bool pertenece=true;
    int indicetab = indiceTabla(nombre);
    if (indicetab == 0) {
        pertenece = false;
    } else {
        if(r != *_tablas[indicetab-1]){
            pertenece = false;
        }
    }
    return pertenece;
}

int BaD::indiceTabla(string &nombre) {
    int cont = 0;
   unsigned long int largo;
    largo = _tnombres.size();

    while (cont < largo) {
        if (nombre == _tnombres[cont]) {
            return cont + 1;
        }
        cont++;
    }
    return 0;
}

bool BaD::chequearNombreCampos(string &nombre, const Registro &r) {
    int indice;
    bool valido;
    indice = indiceTabla(nombre) - 1;

    valido = seteq(r.campos(), _tablas[indice]->campos());


    return valido;
}

bool BaD::chequearTipoDatoCampos(string &tnombre, const Registro &r) {
    int indice;
    indice = indiceTabla(tnombre) - 1;
    vector<string> campos = r.campos();
    bool rTipoDatoCampo, tTipoDatoCampo;
    bool tipos_iguales=true;

    unsigned long int i=0;
    unsigned long int finwhile=campos.size();
    while(i < finwhile) {

        rTipoDatoCampo = r.dato(campos[i]).esNat();
        tTipoDatoCampo = _tablas[indice]->tipoCampo(campos[i]).esNat();

        if (rTipoDatoCampo != tTipoDatoCampo) {
            //se fija si es del mismo tipo el dato del campo
            tipos_iguales = false;
        }
        i++;
    }
    return tipos_iguales;
}

bool BaD::chequearCampos(string &nombre, const Registro &r) {

    bool valido;
    valido = chequearNombreCampos(nombre, r) and chequearTipoDatoCampos(nombre, r);
    return valido;

}

bool BaD::registroPertenece(string &tnombre, const Registro &r) {
    bool pert;
    int indice = indiceTabla(tnombre) - 1;
    pert = pertenece(r, _tablas[indice]->registros());

    return pert;
}

void BaD::agregaRegistroATabla(string &tnombre, Registro &r) {
    if (registroValido(tnombre, r)) {
        int indice = indiceTabla(tnombre) - 1;
        _tablas[indice]->agregarRegistro(r);

    }
}

bool BaD::compararClaves(string &tnombre, const Registro &r) {
    int indice = indiceTabla(tnombre) - 1;
    Tabla *tabla = _tablas[indice];
    vector<string> claves = tabla->claves();

    int i = 0;
    //Iteramos sobre los registros de la tabla.
    while (i < tabla->registros().size()) {
        int j = 0;
        //Inicializamos la flag en false.
        bool datodiferente = false;
        while (j < claves.size()) {
            //Iteramos sobre las claves del registro i.
            //Si alguna clave del registro i de la tabla tiene como dato algo distinto a la clave j del registro r
            //la flag cambia a true, y cuando termina el while sigue iterando buscando el caso donde la clave quede en false, para
            //avisar que encontró un registro cuyas claves coinciden con las del registro que está siendo chequeado.
            //tabla->registros()[i].dato(claves[j]) == dato de la clave j del registro i de la tabla indice de la base de datos baD.
            if (tabla->registros()[i].dato(claves[j]) != r.dato(claves[j])) {
                datodiferente = true;
            }
            j++;

        }
        if (!datodiferente) {
            return false;
        }
        i++;
    }
    return true;

}

bool BaD::registroValido(string &tnombre, const Registro &r) {
    if (chequearCampos(tnombre, r)) {
        return compararClaves(tnombre, r);
    }
}
/*
Tabla tablaConRegistrosConCampoIgualAlDato(string campoRestriccion, Dato valorRestriccion, Tabla tablaOrigen) {


};
*/
vector<Dato> tiposTabla(Tabla *t) {
    vector<Dato> result;
    unsigned long int camposDet = t->campos().size();
    int cont = 0;
    while (cont < camposDet) {
        result.push_back(t->tipoCampo(t->campos()[cont]));
        cont++;
    }
    return result;
}

void meterRegistrosQCumplenRestriccionEnTabla(Tabla &origen, Tabla &resultado, Restriccion &r) {
    string campoDeLaRestriccion = r.campo();
   unsigned long int numeroDeRegistrosEnOrigen = origen.registros().size();
    int registroNesimo = 0;
    while (registroNesimo < numeroDeRegistrosEnOrigen) {

        Dato datoDelRegistroNesimo = origen.registros()[registroNesimo].dato(campoDeLaRestriccion);

        if (datoDelRegistroNesimo == r.valor()) {
            if (r.opcionInclusion()) {
                resultado.agregarRegistro(origen.registros()[registroNesimo]);
            }


        } else {
            if  (!r.opcionInclusion()) {
                resultado.agregarRegistro(origen.registros()[registroNesimo]);
            }
        }

        registroNesimo++;
    }
}

Tabla BaD::busquedaEnTablaConCriterio(string nombreTabla, const Criterio &criterio) {
    int indice = indiceTabla(nombreTabla) - 1;
    unsigned long int indicesDeRestriccionesDelCriterio = criterio.cantidadDeRestricciones();
    int cont = 0;

    vector<string> camposDeLaTabla = _tablas[indice]->campos();
    vector<string> clavesDeLaTabla = _tablas[indice]->claves();
    vector<Dato> tiposDeLaTabla = tiposTabla(_tablas[indice]);

    Tabla resultado(camposDeLaTabla, clavesDeLaTabla, tiposDeLaTabla);

    if (criterioValido(criterio, nombreTabla)) {
        //este if hace que si el criterio es invalido la operacion devuelve una tabla sin registros
        resultado = *_tablas[indice];
        while (cont < indicesDeRestriccionesDelCriterio) {

            Restriccion restriccion = criterio.listaRestricciones()[cont];

            Tabla tablaConRegistrosDeLaBusqueda(camposDeLaTabla, clavesDeLaTabla, tiposDeLaTabla);

            meterRegistrosQCumplenRestriccionEnTabla(resultado,
                                                     tablaConRegistrosDeLaBusqueda,
                                                     restriccion);
            resultado = tablaConRegistrosDeLaBusqueda;
            cont++;
            /*
            campoDeLaRestriccion = criterio.listaRestricciones()[cont].campo();
            if (pertenece(campoDeLaRestriccion, camposDeLaTabla)){
                bool tipoDeLaRestriccionEsNat=criterio.listaRestricciones()[cont].valor().esNat();
                bool tipoDelCampoDeLaTabla = _tablas.[indice]->tipoCampo(campoDeLaRestriccion).esNat();
                if(tipoDeLaRestriccionEsNat == tipoDelCampoDeLaTabla){

                }
            }
            cont++;*/
        }
    }
    return resultado;
}

Tabla BaD::busqueda(string &nombreTabla, const Criterio &criterio) {

        if (criterioValido(criterio, nombreTabla)) {

            _rankBusqueda.agregarBusqueda(criterio);
        }
        return busquedaEnTablaConCriterio(nombreTabla, criterio);
    }



bool restriccionValida(Restriccion &r, Tabla &t) {
    bool valido = true;

    string campoDeLaRestriccion = r.campo();
    if (!pertenece(campoDeLaRestriccion, t.campos())) {
        valido = false;
    } else {
        bool tipoRestriccion = r.valor().esNat();
        bool tipoCampoTabla = t.tipoCampo(campoDeLaRestriccion).esNat();

        if (tipoRestriccion != tipoCampoTabla) {
            valido = false;
        }
    }
    return valido;
}

bool BaD::criterioValido(Criterio const &criterio, string tabla) {
    bool valido = true;
    bool tablaexiste = indiceTabla(tabla)!=0;

    if (tablaexiste) {
        unsigned long int numeroRestricciones = criterio.cantidadDeRestricciones();
        int restriccionActual = 0;
        int numeroTabla = indiceTabla(tabla)-1;

        while (restriccionActual < numeroRestricciones) {
            Restriccion restriccionNesima = criterio.listaRestricciones()[restriccionActual];
            if(!restriccionValida(restriccionNesima, *_tablas[numeroTabla])){
                valido = false;
            };

            restriccionActual++;
        }

    } else {
        valido = false;
    }
    return valido;
}

vector<Criterio> BaD::criterioMasBuscado() {
    return _rankBusqueda.criteriosMasUsados();
}










