//
// Created by fran on 05/09/17.
//

#include "RankBusqueda.h"


void RankBusqueda::agregarBusqueda(Criterio const &c) {
    if (!criterioExiste(c)){
        Criterio pepe = c;
        Busqueda bus(pepe, 1);
        _rankBusqueda.push_back(bus);
    }else{
        int indice = indiceDelCriterioc(c);
        _rankBusqueda[indice].usarBusqueda();
    }
}

vector<Busqueda> RankBusqueda::rankingDeBusqueda() const{
    return _rankBusqueda;
}

Busqueda RankBusqueda::busquedaMasRealizada() {
    Criterio c;
    Busqueda result(c,0 );
   unsigned long int fin = tamanioRanking();
    int i =0;

    int max = i;
    while (i < fin){
        if(_rankBusqueda[i].usos()>_rankBusqueda[max].usos()){
            max= i;
        }
        i++;
    }
    return fin != 0 ? _rankBusqueda[max] : result;


}

int RankBusqueda::indiceDelCriterioc(Criterio const &c) {
   unsigned long int critUsados = _rankBusqueda.size();
    int criterioNesimo = 0;
    bool critNuevo = true;

    while (criterioNesimo < critUsados && critNuevo){
        if (c == _rankBusqueda[criterioNesimo].criterio()){
            critNuevo = false;
        }

        if(critNuevo) {
            criterioNesimo++;
        }
    }
    return critNuevo ? criterioNesimo : -1;
}
//↓↓↓ existe unicamente por claridad ↓↓↓
bool RankBusqueda::criterioExiste(Criterio const &c){
   return indiceDelCriterioc(c) != -1;
}

unsigned long int RankBusqueda::tamanioRanking() const {
    return _rankBusqueda.size();
}

vector<Criterio> RankBusqueda::criteriosMasUsados() {
    vector<Criterio> result;
    unsigned long int fin = tamanioRanking();
    int i =0;

    int max = busquedaMasRealizada().usos();
    while (i < fin){
        if(_rankBusqueda[i].usos()==_rankBusqueda[max].usos()){
            result.push_back(_rankBusqueda[i].criterio());
        }
        i++;
    }
    return result;
}


bool operator==(const RankBusqueda & r1, const RankBusqueda & r2) {
    return seteq(r1.rankingDeBusqueda(),r2.rankingDeBusqueda());
}
