//
// Created by fran on 01/09/17.
//

#include "Restriccion.h"

#include <utility>

Restriccion::Restriccion(string &campo, Dato &valor, bool opcion) :
    _campo (campo),
    _valor(valor),
    _opcion(opcion){};


bool Restriccion::opcionInclusion() const{
    return _opcion;
}


string Restriccion::campo()const {
    return _campo;
}

Dato Restriccion::valor()const {
    return _valor;
}

bool operator==(const Restriccion& r1, const Restriccion& r2) {
    return r1.valor() == r2.valor() and r1.campo()==r2.campo() and r1.opcionInclusion()== r2.opcionInclusion();
}

bool operator!=(const Restriccion& r1, const Restriccion& r2) {
    return !(r1==r2);
}
