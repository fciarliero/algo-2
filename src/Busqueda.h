//
// Created by fran on 05/09/17.
//

#ifndef TPIBASE_BUSQUEDA_H
#define TPIBASE_BUSQUEDA_H
#include "Criterio.h"
#include <vector>

class Busqueda {

public:
    //generador
    Busqueda(Criterio &c, int usos);

    //generador
    void usarBusqueda();

     // obs
    int usos()const;
    //obs
    Criterio criterio()const;



private:
    Criterio _criterio;
    int _usos;
};
bool operator==(const Busqueda&, const Busqueda&);


#endif //TPIBASE_BUSQUEDA_H
