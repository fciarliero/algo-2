//
// Created by fran on 01/09/17.
//

#include "Criterio.h"


void Criterio::agregarRestriccion(Restriccion &r) {
    if(!pertenece(r, _restricciones)){
        _restricciones.push_back(r);
    }
}

vector<Restriccion> Criterio::listaRestricciones() const{
    return _restricciones;
}

unsigned long int Criterio::cantidadDeRestricciones() const {
    return _restricciones.size();
}



bool operator==(const Criterio& c1, const Criterio& c2) {
    return seteq(c1.listaRestricciones(), c2.listaRestricciones());
}

bool operator!=(const Criterio& c1, const Criterio& c2) {
    return !(c1==c2);
}
