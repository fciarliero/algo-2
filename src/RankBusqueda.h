//
// Created by fran on 05/09/17.
//

#ifndef TPIBASE_RANKCRITERIOS_H
#define TPIBASE_RANKCRITERIOS_H

#include "Busqueda.h"
#include "Criterio.h"
#include <vector>


class RankBusqueda {
public:
    //generador
    void agregarBusqueda(Criterio const&c);
    //obs
    vector<Busqueda> rankingDeBusqueda()const;
    //op
    unsigned long int tamanioRanking ()const;
    //op
    vector<Criterio> criteriosMasUsados();
    //op
    Busqueda busquedaMasRealizada();
    //op
    int indiceDelCriterioc (Criterio const &c);
    //op
    bool criterioExiste(Criterio const &c);

private:
    vector<Busqueda> _rankBusqueda;
};

bool operator==(const RankBusqueda&, const RankBusqueda&);


#endif //TPIBASE_RANKCRITERIOS_H
